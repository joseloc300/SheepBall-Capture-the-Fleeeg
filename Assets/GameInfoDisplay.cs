﻿using UnityEngine.UI;
using UnityEngine;

public class GameInfoDisplay : MonoBehaviour
{
    public Text timer;

    public Text redScore;
    public Text blueScore;
    public Text yellowScore;

    private void Start()
    {
        //timer = transform.Find("GameTimer/Text").gameObject;
        //redScore = transform.Find("RedPoints/TeamRedScore").gameObject;
        //blueScore = transform.Find("BluePoints/TeamBlueScore").gameObject;
        //yellowScore = transform.Find("YellowPoints/TeamYellowScore").gameObject;
    }

    public void setTimer(float timeLeft)
    {
        int minutes = (int) timeLeft / 60;
        int seconds = (int) timeLeft % 60;

        string secondsText = (seconds / 10) == 0 ? 0 + seconds.ToString() : seconds.ToString();
        string timerText = minutes + ":" + secondsText;
        timer.GetComponent<Text>().text = timerText;
    }

    public void setRedScore(int score)
    {
        redScore.text = score.ToString();
    }

    public void setBlueScore(int score)
    {
        blueScore.text = score.ToString();
    }

    public void setYellowScore(int score)
    {
        yellowScore.text = score.ToString();
    }
}
