﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuButtonAudio : MonoBehaviour
{

    [FMODUnity.EventRef]
    public string NegativeEvent = "";
    [FMODUnity.EventRef]
    public string PosEvent = "";
    [FMODUnity.EventRef]
    public string Pos2Event = "";

    public void playPos(){
        FMODUnity.RuntimeManager.PlayOneShot(PosEvent, transform.position);
    }
    public void playNeg(){
        FMODUnity.RuntimeManager.PlayOneShot(NegativeEvent, transform.position);
    }
    public void playPos2(){
        FMODUnity.RuntimeManager.PlayOneShot(Pos2Event, transform.position);
    }
}
