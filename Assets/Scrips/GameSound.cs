﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameSound : MonoBehaviour
{
    
    string banjoBusString = "Bus:/Master/Music/Game theme/Banjo";
    string violinBusString = "Bus:/Master/Music/Game theme/Violino";
    string violaoBusString = "Bus:/Master/Music/Game theme/Violao";
    
    FMOD.Studio.Bus banjoBus;
    FMOD.Studio.Bus violinBus;
    FMOD.Studio.Bus violaoBus;

    GameObject gameStateManagerObj;

    float time;

    // Start is called before the first frame update
    void Start()
    {
        setupBuses();
        time = 0;

        gameStateManagerObj = GameObject.FindGameObjectWithTag("GameStateManager");
    }

    // Update is called once per frame
    void Update()
    {
        if(gameStateManagerObj == null)
            gameStateManagerObj = GameObject.FindGameObjectWithTag("GameStateManager");


        time += Time.deltaTime;
        int timeInt = Mathf.FloorToInt(time);
        if (timeInt == 43 || timeInt == 81 || timeInt == 124 || timeInt == 161 || timeInt == 220)
        {
            switchSolos();
        }
    }

    void setupBuses()
    {
        banjoBus = FMODUnity.RuntimeManager.GetBus(banjoBusString);
        banjoBus.setVolume(0.0f);

        violinBus = FMODUnity.RuntimeManager.GetBus(violinBusString);
        violinBus.setVolume(0.0f);

        violaoBus = FMODUnity.RuntimeManager.GetBus(violaoBusString);
        violaoBus.setVolume(0.0f);
    }

    void switchSolos()
    {
        int redScore = gameStateManagerObj.GetComponent<GameStateManager>().teamRedScore;
        int blueScore = gameStateManagerObj.GetComponent<GameStateManager>().teamBlueScore;
        int yellowScore = gameStateManagerObj.GetComponent<GameStateManager>().teamYellowScore;

        int max = Mathf.Max(redScore, blueScore, yellowScore);
        
        if(redScore == max)
        {
            violinBus.setVolume(1.0f);
            banjoBus.setVolume(0.0f);
            violaoBus.setVolume(0.0f);
        }
        else if (blueScore == max)
        {
            banjoBus.setVolume(1.0f);
            violinBus.setVolume(0.0f);
            violaoBus.setVolume(0.0f);
        }
        else
        {
            violaoBus.setVolume(1.0f);
            banjoBus.setVolume(0.0f);
            violinBus.setVolume(0.0f);
        }
    }
}
