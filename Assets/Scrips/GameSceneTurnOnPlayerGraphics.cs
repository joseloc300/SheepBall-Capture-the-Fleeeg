﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PlayerManager;

public class GameSceneTurnOnPlayerGraphics : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < Manager.Instance.ConnectedPlayers.Count; i++)
        {
            Manager.Instance.GetConnectedPlayers()[i].GetComponent<NetworkPlayer>().setGraphicsActive(true);
        }
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
