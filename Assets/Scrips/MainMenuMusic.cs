﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuMusic : MonoBehaviour
{
    public GameObject music;
    
    // Start is called before the first frame update
    void Start()
    {
        DontDestroyOnLoad(this.gameObject);
        GameObject[] go = GameObject.FindGameObjectsWithTag("MainMenuMusic");
        if (go.Length > 1)
            Destroy(this.gameObject);
        else
            music.SetActive(true); 
            
    }

    // Update is called once per frame
    void Update()
    {
        string currentSceneName = SceneManager.GetActiveScene().name;
        if (currentSceneName == "CharacterThirdPerson" || currentSceneName == "PracticeScene")
        {
            Destroy(this.gameObject);
        }
    }
}
