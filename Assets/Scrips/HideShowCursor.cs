﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HideShowCursor : MonoBehaviour
{
    private bool menuActive = false;
    public GameObject menu;
    public GameObject victory;
    public GameObject defeat;

    void Start()
    {
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        menu.SetActive(false);
        victory.SetActive(false);
        defeat.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape)) {
            if (menuActive)
            {
                resume();
            }
            else
            {
                menuActive = true;
                Cursor.visible = true;
                Cursor.lockState = CursorLockMode.None;
                menu.SetActive(true);
            }
        }
    }

    public void resume()
    {
        menu.SetActive(false);
        menuActive = false;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
    }
}
