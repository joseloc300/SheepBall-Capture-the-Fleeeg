﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.Threading;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using PlayerManager;
using SheepAnimationState;

public class SheepAI : NetworkMessageHandler
{
    private List<GameObject> players = new List<GameObject>();
    private List<GameObject> sheeps = new List<GameObject>();
    private List<GameObject> available_sheeps = new List<GameObject>();

    private float timer = TIME_BETWEEN_MOVEMENT;

    private GameObject[] baseWalls;
    
    private const float TIME_BETWEEN_MOVEMENT = 1f;

    [Header("Sheep Movement Properties")]
    public bool canSendNetworkMovement;
    public float speed;
    public float networkSendRate = 5;
    public float timeBetweenMovementStart;
    public float timeBetweenMovementEnd;

    [Header("Lerping Properties")]
    public bool isLerpingPosition;
    public bool isLerpingRotation;
    public Vector3 realPosition;
    public Quaternion realRotation;
    public Vector3 lastRealPosition;
    public Quaternion lastRealRotation;
    public float timeStartedLerping;
    public float timeToLerp;

    void Start()
    {
         baseWalls = GameObject.FindGameObjectsWithTag(Constants.BASE_WALL_TAG);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        //SheepAI only runs in Server
        if (!isServer)
            return;

        timer -= Time.deltaTime;

        if (timer <= 0)
        {
            updateAvailableSheeps();
            sheepMovement();
            resetTimer();
        }
    }

    void Update()
    {
        if (!isServer)
            return;

        if (!canSendNetworkMovement)
        {
            canSendNetworkMovement = true;
            StartCoroutine(StartNetworkSendCooldown());
        }
    }
    private IEnumerator StartNetworkSendCooldown()
    {
        timeBetweenMovementStart = Time.time;
        yield return new WaitForSeconds((1 / networkSendRate));
        SendSheepMovementMessage();
    }

    private void SendSheepMovementMessage()
    {
        timeBetweenMovementEnd = Time.time;

        List<GameObject> sheepsToSend = getSheepsToSend();
        
        SheepMovementMessage _msg = new SheepMovementMessage()
        {
            numAvailableSheep = sheepsToSend.Count,
            sheepNames = getSheepNames(sheepsToSend),
            sheepPositions = getSheepPositions(sheepsToSend),
            sheepRotations = getSheepRotations(sheepsToSend),
            sheepStates = getSheepStates(sheepsToSend),
            sheepAnimationStates = getSheepAnimationStates(sheepsToSend),
            sheepCollisionPlayers = getSheepCollisionPlayers(sheepsToSend),
            time = (timeBetweenMovementEnd - timeBetweenMovementStart)
        };

        NetworkManager.singleton.client.Send(sheep_movement_msg, _msg);

        canSendNetworkMovement = false;
    }

    //this is called from the SheepSpawn script
    public void addSheep(GameObject sheep)
    {
        sheeps.Add(sheep);
    }

    void updateAvailableSheeps()
    {
        available_sheeps.Clear();
        foreach (GameObject sheep in sheeps)
        {
            SheepMovement sheep_movement = sheep.GetComponent<SheepMovement>();
            if (sheep_movement.getState() == (int)State.Available)
            {
                if(sheep_movement.getPickedUpBy() == "")
                {
                    available_sheeps.Add(sheep);
                }
                else
                {
                    Debug.Log("Something went wrong. " + sheep.name + "is available but it is picked up by" + sheep_movement.getPickedUpBy());
                }
                
            }
        }
    }

    //Get the sheeps that are not picked up by anyone (different from being in the Available state)
    public List<GameObject> getSheepsToSend()
    {
        List<GameObject> sheepsToSend = new List<GameObject>();
        foreach (GameObject sheep in sheeps)
        {
            SheepMovement sheep_movement = sheep.GetComponent<SheepMovement>();
            if (sheep_movement.getPickedUpBy() == "")
            {
                sheepsToSend.Add(sheep);
            }
        }
        return sheepsToSend;
    }

    void sheepMovement()
    {
        int numSheepsToMove = getNumSheepsToMove(available_sheeps.Count);  //get the number of sheep to move based on ammount of available sheeps

        if (numSheepsToMove == -1)
            return;

        List<int> randomSheepIndexes = calculateRandomIndexes(available_sheeps, numSheepsToMove);
        GameObject[] sheepsToMove = getSheepsToMove(available_sheeps, randomSheepIndexes);

        moveSheeps(sheepsToMove);
    }

    void resetTimer()
    {
        timer = TIME_BETWEEN_MOVEMENT;
    }

    int getNumSheepsToMove(int totalSheeps)
    {
        if (totalSheeps < 2)
            return -1;
        return 2; //TO DO (% of total Sheeps + random factor)
    }

    List<int> calculateRandomIndexes(List<GameObject> available_sheeps, int numSheepsToMove)
    {
        List<int> randomIndexes = new List<int>();
        System.Random rnd = new System.Random();

        for (int i = 0; i < numSheepsToMove; i++)
        {
            int number;
            do
            {
                number = rnd.Next(0, available_sheeps.Count);
            }
            while (randomIndexes.Contains(number));

            randomIndexes.Add(number);
        }

        return randomIndexes;
    }

    GameObject[] getSheepsToMove(List<GameObject> available_sheeps, List<int> randomSheepIndexes)
    {
        GameObject[] sheepsToMove = new GameObject[randomSheepIndexes.Count];
        for (int i = 0; i < randomSheepIndexes.Count; i++)
            sheepsToMove[i] = available_sheeps[randomSheepIndexes[i]];

        return sheepsToMove;
    }

    void moveSheeps(GameObject[] sheepsToMove)
    {
        foreach (GameObject sheep in sheepsToMove)
        {
            var sheep_script = sheep.GetComponent<SheepMovement>();
            StartCoroutine(sheep_script.move());
        }
    }

    public string[] getSheepNames(List<GameObject> list)
    {
        int num_sheep = list.Count;
        string[] sheepNames = new string[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepNames[i] = list[i].transform.name;
        }

        return sheepNames;
    }

    public Vector3[] getSheepPositions(List<GameObject> list)
    {
        int num_sheep = list.Count;
        Vector3[] sheepPositions = new Vector3[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepPositions[i] = list[i].transform.position;
        }

        return sheepPositions;
    }

    public Quaternion[] getSheepRotations(List<GameObject> list)
    {
        int num_sheep = list.Count;
        Quaternion[] sheepRotations = new Quaternion[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepRotations[i] = list[i].transform.rotation;
        }

        return sheepRotations;
    }

    public int[] getSheepStates(List<GameObject> list)
    {
        int num_sheep = list.Count;
        int[] sheepStates = new int[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepStates[i] = list[i].GetComponent<SheepMovement>().getState();
        }

        return sheepStates;
    }

    public int[] getSheepAnimationStates(List<GameObject> list)
    {
        int num_sheep = list.Count;
        int[] sheepAnimationStates = new int[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepAnimationStates[i] = list[i].GetComponentInChildren<Animator>().GetInteger("Index");
        }

        return sheepAnimationStates;
    }

    public bool[] getSheepCollisionPlayers(List<GameObject> list)
    {
        int num_sheep = list.Count;
        bool[] sheepCollisionPlayers = new bool[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepCollisionPlayers[i] = list[i].GetComponent<SheepMovement>().getIgnoreCollisionPlayers();
        }

        return sheepCollisionPlayers;
    }

    private void ignoreCollisionWithBases(GameObject sheep, bool ignore)
    {
        foreach (GameObject wall in baseWalls)
        {
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), wall.GetComponent<Collider>(), ignore);
        }
    }

    private void ignoreCollisionWithPlayers(GameObject sheep, bool ignore)
    {
        GameObject player;
        foreach (GameObject p in Manager.Instance.GetConnectedPlayers())
        {
            player = p.transform.GetChild(p.GetComponent<NetworkPlayer>().team - 1).gameObject;
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), player.GetComponent<Collider>(), ignore);
        }
    }

    /*bool hasValue(int[] array, int value)
    {
        if (array.Length == 0)
            return false;

        for(int i = 0; i < array.Length; i++)
        {
            if (array[i] == value)
                return true;
        }
        return false;
    }*/
}
