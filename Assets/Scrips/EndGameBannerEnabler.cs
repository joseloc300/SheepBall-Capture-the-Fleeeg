﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EndGameBannerEnabler : MonoBehaviour
{

    public GameObject victoryBanner;
    public GameObject defeatBanner;

    GameObject SfxPlayer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (SfxPlayer == null)
            SfxPlayer = GameObject.FindGameObjectWithTag("SFXPlayer");
    }

    public void enableDefeatBanner()
    {
        defeatBanner.active = true;
    }

    public void enableVictoryBanner()
    {
        victoryBanner.active = true;
        SfxPlayer.GetComponent<SFXPlayer>().playWinAnnoucerEvent();
    }
}
