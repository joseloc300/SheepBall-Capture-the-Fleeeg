﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityEngine.Networking;
using SheepAnimationState;
using PlayerManager;

public class PickupSheep : NetworkMessageHandler
{
    List<GameObject> sheepColliding;
    Stack<GameObject> sheepPickedup;
    ThirdPersonUserControl userControls;
    GameObject[] baseWalls;
    Animator child_Animator;

    [FMODUnity.EventRef]
    public string pickUpSheepEvent = "";

    GameObject SfxPlayer;

    private void Awake()
    {
        userControls = GetComponent<ThirdPersonUserControl>();
        sheepPickedup = new Stack<GameObject>();
        sheepColliding = new List<GameObject>();
        baseWalls = GameObject.FindGameObjectsWithTag(Constants.BASE_WALL_TAG);
        Debug.Log("base walls: " + baseWalls.Length);
        child_Animator = GetComponentsInChildren<Animator>()[1];
    }

    void Update()
    {

        if (SfxPlayer == null)
            SfxPlayer = GameObject.FindGameObjectWithTag("SFXPlayer");

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (sheepColliding.Count > 0 && sheepPickedup.Count < Constants.MAX_SHEEP_CARRIED)
            {
                pickupSheep();
            }
            else
            {
                Debug.Log("VAI DROPAR");
                StartCoroutine(dropSheep());
            }
        }

        updateSheepRotation();
    }

    private void OnTriggerEnter(Collider body)
    {
        Debug.Log("OnTriggerEnter " + transform.parent.gameObject.name);
        Debug.Log("Manager Local Player ID:  " + Manager.Instance.PlayerID);
        if (transform.parent.gameObject.name != Manager.Instance.PlayerID)
            return;

        if (body.tag == Constants.SHEEP_TAG)
        {
            var sheep = body.gameObject;
            if (!sheepPickedup.Contains(sheep)) //prevent bug due to collider being too big (sheep collides with player while being carried by him)
            {
                sheepColliding.Add(sheep);
                Debug.Log(sheep.transform.name + "added sheep to colliding");
            }

        }
    }

    private void OnTriggerExit(Collider body)
    {
        if (transform.parent.gameObject.name != Manager.Instance.PlayerID)
            return;

        if (body.tag == Constants.SHEEP_TAG)
        {
            for (var i = 0; i < sheepColliding.Count; i++)
            {
                if (GameObject.ReferenceEquals(body.gameObject, sheepColliding[i]))
                {
                    Debug.Log(body.gameObject.transform.name + " ontrigger exit");
                    sheepColliding.RemoveAt(i);
                }
            }
        }
    }

    private void changePlayerSpeed()
    {
        switch (sheepPickedup.Count)
        {
            case 0:
                userControls.changeWalkSpeed(1f);
                break;
            case 1:
                userControls.changeWalkSpeed(1f);
                break;
            case 2:
                userControls.changeWalkSpeed(0.90f);
                break;
            case 3:
                userControls.changeWalkSpeed(0.80f);
                break;
        }
    }

    private void pickupSheep()
    {
        FMODUnity.RuntimeManager.PlayOneShot(pickUpSheepEvent, transform.position);

        if (SfxPlayer != null)
            SfxPlayer.GetComponent<SFXPlayer>().playPickUpSheepAnnouncerEvent(transform);     

        GameObject sheepToPickup = sheepColliding[0];
        sheepColliding.RemoveAt(0);

        pickupAux(sheepToPickup);

        sheepPickedup.Push(sheepToPickup);
        changePlayerSpeed();

        if (sheepPickedup.Count == 1)
        {
            child_Animator.SetBool("HoldingSheep", true);
        }
        else if(sheepPickedup.Count == 3)
        {
            if (SfxPlayer != null)
                SfxPlayer.GetComponent<SFXPlayer>().playPickUp3SheepAnnoucerEvent(transform);
        }
    }

    public void pickupAux(GameObject sheepToPickup)
    {
        SheepMovement sheepToPickup_movement = sheepToPickup.GetComponentInChildren<SheepMovement>();
        Animator sheepToPickup_animator = sheepToPickup.GetComponentInChildren<Animator>();
        Rigidbody sheepToPickup_rb = sheepToPickup.GetComponent<Rigidbody>();
        Debug.Log("PICKEI A OVELHA: " + sheepToPickup.name);
        // STATE 
        sheepToPickup_movement.setUnavailable();

        // ANIM STATE
        IAnimState animState = new Ball(ref sheepToPickup_animator);
        sheepToPickup_movement.SetAnimState(animState);

        //PICKED UP BY
        sheepToPickup_movement.setPickedUpBy(transform.name);

        //GRAVITY
        sheepToPickup_rb.velocity = Vector3.zero;
        sheepToPickup_rb.angularVelocity = Vector3.zero;
        sheepToPickup_rb.useGravity = false;
        sheepToPickup_rb.isKinematic = true;
        sheepToPickup_movement.setGravity(false);

        //COLLISION BASES
        ignoreCollisionBases(sheepToPickup, true);
        sheepToPickup_movement.setIgnoreCollisionBases(true);

        //COLLISION PLAYERS
        ignoreCollisionPlayers(sheepToPickup, true);
        sheepToPickup_movement.setIgnoreCollisionPlayers(true);
    }

    public IEnumerator dropSheep()
    {
        if (sheepPickedup.Count == 0)
        {
            yield return -1f;
        }
            
        GameObject droppedSheep = sheepPickedup.Pop();
        GameObject player = GameObject.Find(Manager.Instance.PlayerID).gameObject;
        NetworkPlayer network_player = player.GetComponent<NetworkPlayer>();
        
        network_player.numAcksDrop = 1;
        network_player.canSendNetworkMovement = false; 
        yield return new WaitForSeconds(0.2f);//ESPERAR QUE MANDE TODAS AS MENSAGENS PLAYER INFO
        
        SendDropSheepMessage(network_player.name, droppedSheep.name);

        changePlayerSpeed();

        if (sheepPickedup.Count == 0)
        {
            child_Animator.SetBool("HoldingSheep", false);
        }
    }

    public IEnumerator dropAllSheep()
    {
        if (sheepPickedup.Count == 0)
            yield return -1f;

        GameObject player = GameObject.Find(Manager.Instance.PlayerID).gameObject;
        NetworkPlayer network_player = player.GetComponent<NetworkPlayer>();

        network_player.numAcksDrop = sheepPickedup.Count;
        network_player.canSendNetworkMovement = false;
        yield return new WaitForSeconds(0.2f);//ESPERAR QUE MANDE TODAS AS MENSAGENS PLAYER INFO

        for (int i = 0; i < sheepPickedup.Count; i++)
        {
            GameObject droppedSheep = sheepPickedup.Pop();
            SendDropSheepMessage(network_player.name, droppedSheep.name);
        }

        changePlayerSpeed();

        if (sheepPickedup.Count == 0)
        {
            child_Animator.SetBool("HoldingSheep", false);
        }
    }

    public void SendDropSheepMessage(string playerName, string _sheepID)
    {
        DropSheepMessage _msg = new DropSheepMessage()
        {
            playerName = playerName,
            sheepName = _sheepID
        };

        NetworkManager.singleton.client.Send(drop_sheep_message, _msg);
    }

    public void prepareToShoot()
    {
        if (sheepPickedup.Count == 0)
        {
            child_Animator.SetTrigger("Last_Shot");
            child_Animator.SetBool("HoldingSheep", false);
        }
        else
            child_Animator.SetTrigger("Shooting");

        changePlayerSpeed();
    }

    public void updateSheepRotation()
    {
        if (sheepPickedup.Count == 0) return;

        int index = 0;
        foreach (GameObject sheep in sheepPickedup)
        {
            Vector3 newV = gameObject.transform.position;
            Vector3 oldNewV = newV;
            Quaternion newR = gameObject.transform.rotation;
            newV.y += 2.1f;
            newV.x -= 1f;
            switch (index++)
            {
                case 0:
                    break;
                case 1:
                    newV.y += 0.8f;
                    break;
                case 2:
                    newV.y += 1.6f;
                    break;
            }
            sheep.transform.position = newV;
            sheep.transform.rotation = newR;
            sheep.transform.RotateAround(newV, Vector3.up, -newR.eulerAngles.y);
            sheep.transform.RotateAround(oldNewV, Vector3.up, newR.eulerAngles.y);

            sheep.transform.Rotate(new Vector3(0, 180, 0),Space.Self);

            //this prevents "teleporting" after dropping sheep on the client, since the sheep position now will be updated
            // according to sheep movement message, instead of being updated in this function
            // the following values will not actulally be used for lerping, but this way they will be updated for when the sheep is dropped
            SheepMovement sheep_movement = sheep.GetComponent<SheepMovement>();
            sheep_movement.lastRealPosition = sheep_movement.realPosition;
            sheep_movement.lastRealRotation = sheep_movement.realRotation;
            sheep_movement.realPosition = sheep.transform.position;
            sheep_movement.realRotation = sheep.transform.rotation;
        }
    }

    public Stack<GameObject> getSheepStack()
    {
        return sheepPickedup;
    }

    public List<GameObject> getSheepColliding()
    {
        return sheepColliding;
    }

    public GameObject[] getBaseWalls()
    {
        return baseWalls;
    }

    // ignore = true means: Avoid sheep from colliding with base walls
    // ignore = false means: Allow sheep to collide with base walls
    private void ignoreCollisionBases(GameObject sheep, bool ignore)
    {
        foreach (GameObject wall in baseWalls)
        {
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), wall.GetComponent<Collider>(), ignore);
        }
    }

    // ignore = true means: Avoid sheep from colliding with players
    // ignore = false means: Allow sheep to collide with players
    public void ignoreCollisionPlayers(GameObject sheep, bool ignore)
    {
        GameObject player;
        foreach (GameObject p in Manager.Instance.GetConnectedPlayers())
        {
            player = p.transform.GetChild(p.GetComponent<NetworkPlayer>().team - 1).gameObject;
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), player.GetComponent<Collider>(), ignore);
        }
    }
}
