﻿using UnityEngine;

public class SheepCaptureState : MonoBehaviour
{
    private bool captured = false;
    private GameObject captureBase = null;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public bool isCaptured()
    {
        return this.captured;
    }

    public void capture(GameObject captureBase)
    {
        this.captured = true;
        this.captureBase = captureBase;

        Color baseColor = captureBase.GetComponent<SheepBase>().GetColor();
        GameObject sheepBody = transform.Find("Sheep/Sphere.223").gameObject;
        sheepBody.GetComponent<Renderer>().material.SetColor("_Color", baseColor);
    }

    public void steal() // catpured state is false, but the previous base is kept
    {
        this.captured = false;
    }

    public void dropSteal() // drop a sheep while it is being stolen (only possible through a stun)
    {
        this.captured = false;
        this.captureBase = null;

        GameObject sheepBody = transform.Find("Sheep/Sphere.223").gameObject;
        sheepBody.GetComponent<Renderer>().material.SetColor("_Color", Constants.DEFAULT);
    }
}
