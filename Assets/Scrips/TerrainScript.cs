﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SheepAnimationState;

public class TerrainScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == Constants.SHEEP_TAG)
        {
            SheepMovement sheep_movement = collision.collider.gameObject.GetComponent<SheepMovement>();
            Rigidbody sheep_rb = collision.collider.gameObject.GetComponent<Rigidbody>();
            if (sheep_movement.getState() == (int)State.Flying)
            {
                sheep_movement.setAvailable();
                sheep_rb.isKinematic = true;
                sheep_rb.isKinematic = false;
                sheep_movement.ignoreCollisionWithPlayers(collision.collider.gameObject, true);
                sheep_movement.setIgnoreCollisionPlayers(true);
            }
        }
    }
}
