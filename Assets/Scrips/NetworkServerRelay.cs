﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using PlayerManager;
using SheepAnimationState;

public class NetworkServerRelay : NetworkMessageHandler
{
    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        if (isServer)
        {
            RegisterNetworkMessages();
        }
    }

    private void RegisterNetworkMessages()
    {
        NetworkServer.RegisterHandler(lobby_info_msg, OnReceiveLobbyInfoMessage);
        NetworkServer.RegisterHandler(join_team_msg, OnReceiveJoinTeamMessage);
        NetworkServer.RegisterHandler(match_info_msg, SendmatchInfo);
        NetworkServer.RegisterHandler(leave_lobby_msg, OnReceiveLeaveLobbyMessage);
        NetworkServer.RegisterHandler(end_game_msg, SendEndGameMessage);

        NetworkServer.RegisterHandler(player_info_msg, SendPlayerInfo);
        NetworkServer.RegisterHandler(sheep_movement_msg, SendSheepMovement);
        NetworkServer.RegisterHandler(shoot_sheep_message, PerformShot);
        NetworkServer.RegisterHandler(drop_sheep_message, PerformDrop);
    }

    private void OnReceiveLobbyInfoMessage(NetworkMessage _message)
    {
        LobbyInfoMessage _msg = _message.ReadMessage<LobbyInfoMessage>();
        NetworkServer.SendToReady(null, lobby_info_msg, _msg);
    }

    private void OnReceiveJoinTeamMessage(NetworkMessage _message)
    {
        JoinTeamMessage _msg = _message.ReadMessage<JoinTeamMessage>();
        NetworkServer.SendToReady(null, join_team_msg, _msg);
    }

    private void OnReceiveLeaveLobbyMessage(NetworkMessage _message)
    {
        Debug.Log("Server received LeaveLobbyMessage");
        LeaveLobbyMessage _msg = _message.ReadMessage<LeaveLobbyMessage>();
        NetworkServer.SendToReady(null, leave_lobby_msg, _msg);
    }

    public void SendmatchInfo(NetworkMessage _message)
	{
        MatchInfoMessage _msg = _message.ReadMessage<MatchInfoMessage>();
        NetworkServer.SendToReady(null, match_info_msg, _msg);
    }

    public void SendEndGameMessage(NetworkMessage _message)
    {
        EndGameMessage _msg = _message.ReadMessage<EndGameMessage>();
        NetworkServer.SendToReady(null, end_game_msg, _msg);
    }

    private void SendPlayerInfo(NetworkMessage _message)
    {
        PlayerInfoMessage _msg = _message.ReadMessage<PlayerInfoMessage>();
        NetworkServer.SendToReady(null, player_info_msg, _msg);
    }

    public void SendSheepMovement(NetworkMessage _message)
    {
        SheepMovementMessage _msg = _message.ReadMessage<SheepMovementMessage>();
        NetworkServer.SendToReady(null, sheep_movement_msg, _msg);
    }

    public void PerformShot(NetworkMessage _message)
    {
        ShootSheepMessage _msg = _message.ReadMessage<ShootSheepMessage>();

        GameObject sheep = GameObject.Find(_msg.sheepName).gameObject;
        Animator sheep_animator = sheep.GetComponentInChildren<Animator>();
        SheepMovement sheep_movement = sheep.GetComponent<SheepMovement>();
        Rigidbody sheep_rb = sheep.GetComponent<Rigidbody>();

        //STATE
        sheep_movement.setFlying();

        //ANIMATION STATE
        IAnimState animState = new Shot(ref sheep_animator);
        sheep_movement.SetAnimState(animState);

        //PICKED UP BY
        sheep_movement.setPickedUpBy("");

        //GRAVITY
        sheep_rb.velocity = Vector3.zero;
        sheep_rb.useGravity = true;
        sheep_rb.isKinematic = false;
        sheep_movement.setGravity(true);

        //COLLISION BASES
        sheep_movement.ignoreCollisionWithBases(sheep, false);
        sheep_movement.setIgnoreCollisionBases(false);

        //COLLISION PLAYERS
        sheep_movement.ignoreCollisionWithPlayers(sheep, false);
        sheep_movement.setIgnoreCollisionPlayers(false);

        //POSITION AND ROTATION
        sheep.transform.position = _msg.sheepPosition;
        sheep.transform.rotation = _msg.sheepRotation;
        sheep_rb.position = _msg.sheepPosition;
        sheep_rb.rotation = _msg.playerRotation;
        Debug.Log("POSITION ON SERVER: " + sheep.transform.position);
        Debug.Log("POSITION ON SERVER (that came form messgae): " + _msg.sheepPosition);

        sheep_rb.AddRelativeForce(new Vector3(0, 0.1f, 1.2f) * _msg.impulse, ForceMode.Impulse);
        
        AckShotMessage ack_msg = new AckShotMessage()
        {
            playerName = _msg.playerName,
        };
        Debug.Log("GOING TO SEND ACK SHOT: " + ack_msg.playerName);
        NetworkServer.SendToReady(null, ack_shot_msg, ack_msg);
    }

    public void PerformDrop(NetworkMessage _message)
    {
        DropSheepMessage _msg = _message.ReadMessage<DropSheepMessage>();

        GameObject sheep = GameObject.Find(_msg.sheepName).gameObject;

        SheepMovement sheep_movement = sheep.GetComponentInChildren<SheepMovement>();
        Animator sheep_animator = sheep.GetComponentInChildren<Animator>();
        Rigidbody sheep_rb = sheep.GetComponent<Rigidbody>();

        //STATE
        sheep_movement.setAvailable();

        //ANIM STATE
        IAnimState animState = new Iddle(ref sheep_animator);
        sheep_movement.SetAnimState(animState);

        //PICKED UP BY
        sheep_movement.setPickedUpBy("");

        //GRAVITY
        sheep_rb.velocity = Vector3.zero;
        sheep_rb.useGravity = true;
        sheep_rb.isKinematic = false;
        sheep_movement.setGravity(true);

        //COLLISION BASES
        sheep_movement.ignoreCollisionWithBases(sheep, false);
        sheep_movement.setIgnoreCollisionBases(false);

        //COLLISION PLAYERS
        sheep_movement.ignoreCollisionWithPlayers(sheep, true);
        sheep_movement.setIgnoreCollisionPlayers(true);

        AckDropMessage ack_msg = new AckDropMessage()
        {
            playerName = _msg.playerName,
        };

        NetworkServer.SendToReady(null, ack_drop_msg, ack_msg);
    }
}
