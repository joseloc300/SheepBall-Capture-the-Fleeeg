﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;

public abstract class NetworkMessageHandler : NetworkBehaviour
{
    public const short player_info_msg = 1000;
    public const short sheep_movement_msg = 1001;
    public const short drop_sheep_message = 1002;
    public const short ack_drop_msg = 1003;
    public const short shoot_sheep_message = 1004;
    public const short ack_shot_msg = 1005;
    public const short match_info_msg = 1006;
    public const short join_team_msg = 1007;
    public const short lobby_info_msg = 1008;
    public const short leave_lobby_msg = 1009;
    public const short end_game_msg = 1010;

    public class PlayerInfoMessage : MessageBase
    {
        public string playerName;
        public Vector3 playerPosition;
        public Quaternion playerRotation;
        public int playerAnimationState;
        public int numSheepCarried;
        public string[] sheepNames;
        public Vector3[] sheepPositions;
        public Quaternion[] sheepRotations;
        public int[] sheepStates;
        public int[] sheepAnimationStates;
        public float time;
    }

    public class SheepMovementMessage : MessageBase
    {
        public int numAvailableSheep;
        public string[] sheepNames;
        public Vector3[] sheepPositions;
        public Quaternion[] sheepRotations;
        public int[] sheepStates;
        public int[] sheepAnimationStates;
        public bool[] sheepCollisionPlayers;
        public float time;
    }

    public class PickedUpSheepMessage : MessageBase
    {
        public string playerName;
        public string sheepName;
        public Vector3 sheepPosition;
        public Quaternion sheepRotation;
        public float time;
        public int sheepState;
        public int sheepAnimation;
    }
    public class DropSheepMessage : MessageBase
    {
        public string playerName;
        public string sheepName;
    }
    public class AckDropMessage : MessageBase
    {
        public string playerName;
    }
    public class ShootSheepMessage : MessageBase
    {
        public string playerName;
        public Quaternion playerRotation;
        public Vector3 sheepPosition;
        public Quaternion sheepRotation;
        public string sheepName;
        public float impulse;
    }
    public class AckShotMessage : MessageBase
    {
        public string playerName;
    }

    public class LandSheepMessage : MessageBase
    {
        public string sheepName;
    }

    public class MatchInfoMessage : MessageBase
    {
        public int scoreTeamRed;
        public int scoreTeamBlue;
        public int scoreTeamYellow;
        public float time;
    }

    public class JoinTeamMessage : MessageBase
    {
        public string objectTransformName;
        public string nickname;
        public int team;
    }

    public class LeaveLobbyMessage : MessageBase
    {
        public string objectTransformName;
    }

    public class LobbyInfoMessage : MessageBase
    {
        public bool hasPlayer0;
        public string player0transformName;

        public bool hasPlayer1;
        public string player1transformName;

        public bool hasPlayer2;
        public string player2transformName;

        public bool hasPlayer3;
        public string player3transformName;

        public bool hasPlayer4;
        public string player4transformName;

        public bool hasPlayer5;
        public string player5transformName;

    }

    public class EndGameMessage : MessageBase
    {
        public bool hasPlayer0;
        public string player0transformName;
        public bool player0Won;

        public bool hasPlayer1;
        public string player1transformName;
        public bool player1Won;

        public bool hasPlayer2;
        public string player2transformName;
        public bool player2Won;

        public bool hasPlayer3;
        public string player3transformName;
        public bool player3Won;

        public bool hasPlayer4;
        public string player4transformName;
        public bool player4Won;

        public bool hasPlayer5;
        public string player5transformName;
        public bool player5Won;

    }

}
