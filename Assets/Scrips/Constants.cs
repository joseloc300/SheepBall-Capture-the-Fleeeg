﻿using UnityEngine;

public static class Constants
{
    public const string SHEEP_TAG = "Sheep";
    public const string PLAYER_TAG = "Player";
    public const string BASE_WALL_TAG = "BaseWall";
    public const string BASE_TAG = "Base";
    public const string SPAWN_AREA_NAME = "SpawnArea";
    public const int MAX_NEUTRAL_SHEEP = 20;
    public const int MAX_SHEEP_CARRIED = 3;
    public const float SPAWN_COOLDOWN = 3.0f;
    public static Color RED = new Color(0.8396f, 0.0831f, 0.1322f, 0.357f);
    public static Color BLUE = new Color(0.1333f, 0.0831f, 0.8392f, 0.357f);
    public static Color YELLOW = new Color(0.8392f, 0.7194f, 0.0823f, 0.357f);
    public static Color DEFAULT = new Color(0.6431f, 0.5647f, 0.4471f, 0.357f);
}