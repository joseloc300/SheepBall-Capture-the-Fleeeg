﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

public class StartMenuScript : MonoBehaviour
{
    string masterBusString = "Bus:/Master";
    string musicBusString = "Bus:/Master/Music";
    string sfxBusString = "Bus:/Master/SFX";
    FMOD.Studio.Bus masterBus;
    FMOD.Studio.Bus musicBus;
    FMOD.Studio.Bus sfxBus;

    void Start()
    {
        int fullscreen = PlayerPrefs.GetInt("Fullscreen");
        string resolution = PlayerPrefs.GetString("Resolution");
        if (PlayerPrefs.HasKey("Resolution"))
        {
            resolution = Screen.currentResolution.width + "x" + Screen.currentResolution.height;
            PlayerPrefs.SetString("Resolution", resolution);
        }
        else
        {
            string[] values = resolution.Split('x');
            int horRes = -1;
            int verRes = -1;
            if (fullscreen == 0)
                if(int.TryParse(values[0], out horRes) && int.TryParse(values[1], out verRes))
                    Screen.SetResolution(horRes, verRes, false);
            else
                if (int.TryParse(values[0], out horRes) && int.TryParse(values[1], out verRes))
                    Screen.SetResolution(horRes, verRes, true);
        }
        
        checkerSetter("masterVolume", masterBusString, masterBus);
        checkerSetter("musicVolume", musicBusString, musicBus);
        checkerSetter("sfxVolume", sfxBusString, sfxBus);
    }

    void checkerSetter(string key, string busString, FMOD.Studio.Bus bus)
    {
        if (PlayerPrefs.HasKey(key))
        {
            bus = FMODUnity.RuntimeManager.GetBus(busString);
            sfxBus.setVolume(PlayerPrefs.GetFloat(key));
        }
        else
        {
            PlayerPrefs.SetFloat(key, 1);
        }
    }
    
}
