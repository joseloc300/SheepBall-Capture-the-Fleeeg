﻿using System.Collections.Generic;
using UnityEngine;

public class SheepBase : MonoBehaviour
{
    public enum BaseColor
    {
        Red,
        Blue,
        Yellow
    };
    public BaseColor color;

    public GameObject gameStateManager;

    public List<GameObject> capturedSheep;

    // Start is called before the first frame update
    void Awake()
    {
        this.capturedSheep = new List<GameObject>();

        // Set base color
        GetComponent<Renderer>().material.SetColor("_Color", this.GetColor());
        this.gameStateManager = GameObject.FindGameObjectWithTag("GameStateManager");

    }

    // Update is called once per frame
    void Update()
    {
        if (this.gameStateManager == null)
            this.gameStateManager = GameObject.FindGameObjectWithTag("GameStateManager");
    }

    public Color GetColor()
    {
        switch (this.color)
        {
            case BaseColor.Red:
                return Constants.RED;
            case BaseColor.Blue:
                return Constants.BLUE;
            default:
                return Constants.YELLOW;
        }
    }

    private bool isSheepCaptured(GameObject sheep)
    {
        for (var i = 0; i < capturedSheep.Count; i++)
        {
            if (GameObject.ReferenceEquals(sheep, capturedSheep[i]))
            {
                return true;
            }
        }

        return false;
    }

    private void OnTriggerEnter(Collider body)
    {
        Debug.Log(body.tag + " ENTROU NA BASE");

        // If sheep has entered base
        if (body.tag == Constants.SHEEP_TAG && !isSheepCaptured(body.gameObject))
        {
            Debug.Log("FOI UMA OVELHA");
            GameObject sheep = body.gameObject;
            sheep.GetComponent<SheepCaptureState>().capture(this.gameObject);
            this.capturedSheep.Add(sheep);
        }
    }

    private void OnTriggerExit(Collider body)
    {
        Debug.Log(body.tag + " Saiu da base");
        // If sheep has exited base
        if (body.tag == Constants.SHEEP_TAG && isSheepCaptured(body.gameObject))
        {
            Debug.Log("FOI UMA OVELHA");
            GameObject sheep = body.gameObject;
            sheep.GetComponent<SheepCaptureState>().steal();

            for (var i = 0; i < capturedSheep.Count; i++)
            {
                if (GameObject.ReferenceEquals(sheep, capturedSheep[i]))
                {
                    capturedSheep.RemoveAt(i);
                }
            }
        }
    }

    public int getScore()
    {
        return capturedSheep.Count;
    }
}