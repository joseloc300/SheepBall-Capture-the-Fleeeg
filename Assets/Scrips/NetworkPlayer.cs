using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.Networking.NetworkSystem;
using PlayerManager;
using UnityStandardAssets.Characters.ThirdPerson;
using UnityStandardAssets.Cameras;
using SheepAnimationState;
using UnityEngine.SceneManagement;

public class NetworkPlayer : NetworkMessageHandler
{
    Vector3 spawnPlayer0 = new Vector3(-38, 1, 5);
    Vector3 spawnPlayer1 = new Vector3(-38, 1, -4);
    Vector3 spawnPlayer2 = new Vector3(23, 1, 30);
    Vector3 spawnPlayer3 = new Vector3(16, 1, 35);
    Vector3 spawnPlayer4 = new Vector3(15, 1, -35);
    Vector3 spawnPlayer5 = new Vector3(22, 1, -30);

    [Header("Player Properties")]
    public string playerID;
    public int team = 1;
    public int teamIndex = 0;

    [Header("Player Movement Properties")]
    public bool canSendNetworkMovement;
    public float networkSendRate = 5;
    public float timeBetweenMovementStart;
    public float timeBetweenMovementEnd;

    [Header("Lerping Properties")]
    public bool isLerpingPosition;
    public bool isLerpingRotation;
    public Vector3 realPosition;
    public Quaternion realRotation;
    public Vector3 lastRealPosition;
    public Quaternion lastRealRotation;
    public float timeStartedLerping;
    public float timeToLerp;

    private GameObject sheepManager;

    public GameObject gameStateManagerObj;

    public GameObject networkServerRelayPrefab;
    public GameObject gameStateManagerPrefab;

    private Animator m_animator;

    public int numAcksDrop = 0;
    public int numAcksShoot = 0;

    private void Start()
    {
        DontDestroyOnLoad(this.gameObject);

        if(isServer)
        {
            if(GameObject.FindGameObjectWithTag("NetworkServerRelay") == null)
            {
                GameObject go = Instantiate(networkServerRelayPrefab);
                NetworkServer.Spawn(go);
            }

            if (GameObject.FindGameObjectWithTag("GameStateManager") == null)
            {
                GameObject go1 = Instantiate(gameStateManagerPrefab);
                NetworkServer.Spawn(go1);
            }

        }

        playerID = "player" + GetComponent<NetworkIdentity>().netId.ToString();
        transform.name = playerID;
        Manager.Instance.AddPlayerToConnectedPlayers(playerID, this.gameObject);

        if (isLocalPlayer)
        {
            Manager.Instance.SetLocalPlayerID(playerID);

            sheepManager = GameObject.FindGameObjectWithTag("SheepManager");
            canSendNetworkMovement = false;

            transform.GetComponentInChildren<FreeLookCam>().SetTarget(transform.GetChild(team - 1).transform);
            transform.GetComponentInChildren<FreeLookCam>().RealTarget = transform.GetChild(team - 1).transform;
        }
        else
        {
            disableAllScripts();

            //inicializar variaveis lerp
            isLerpingPosition = false;
            isLerpingRotation = false;

            realPosition = this.transform.GetChild(team - 1).transform.position;
            realRotation = this.transform.GetChild(team - 1).transform.rotation;
        }

        setGraphicsActive(false);
    }

    public override void OnStartLocalPlayer()
    {
        base.OnStartLocalPlayer();
        RegisterNetworkMessages();
    }

    //associar o receber de uma dada mensagem a um handler
    private void RegisterNetworkMessages()
    {
        //player info
        NetworkManager.singleton.client.RegisterHandler(player_info_msg, OnReceivePlayerInfoMessage);

        //ack drop
        NetworkManager.singleton.client.RegisterHandler(ack_drop_msg, OnReceiveAckDropMessage);

        //ack shot
        NetworkManager.singleton.client.RegisterHandler(ack_shot_msg, OnReceiveAckShotMessage);

        //sheep movement
        NetworkManager.singleton.client.RegisterHandler(sheep_movement_msg, OnReceiveSheepMovementMessage);

		//join team
        NetworkManager.singleton.client.RegisterHandler(join_team_msg, OnReceiveJoinTeamMessage);

        //lobby info
        NetworkManager.singleton.client.RegisterHandler(lobby_info_msg, OnReceiveLobbyInfoMessage);

        //match info
        NetworkManager.singleton.client.RegisterHandler(match_info_msg, OnReceiveMatchInfoMessage);

        //leave lobby
        NetworkManager.singleton.client.RegisterHandler(leave_lobby_msg, OnReceiveLeaveLobbyMessage);

        //end game
        NetworkManager.singleton.client.RegisterHandler(end_game_msg, OnReceiveEndGameMessage);
    }

    // desativar todos os scripts dos jogadores nao locais
    private void disableAllScripts()
    {
        GetComponentInChildren<ThirdPersonUserControl>().enabled = false;
        GetComponentInChildren<ThirdPersonCharacter>().enabled = false;
        GetComponentInChildren<PickupSheep>().enabled = false;
        GetComponentInChildren<ShootSheep>().enabled = false;
        GetComponentInChildren<FreeLookCam>().enabled = false;
        GetComponentInChildren<ProtectCameraFromWallClip>().enabled = false;
        GetComponentInChildren<Camera>().enabled = false;
    }

    //recebe do servidor um ack a dizer que ja dropou a sua ovelha
    private void OnReceiveAckDropMessage(NetworkMessage _message)
    {
        AckDropMessage _msg = _message.ReadMessage<AckDropMessage>();

        if(_msg.playerName == transform.name)//QUERO ACKs APENAS SOBRE OVELHAS QUE DEI DROP
        {
            numAcksDrop--;
            if(numAcksDrop == 0)
            {
                Debug.Log("Reactivanting Sending PlayerInfo");
                canSendNetworkMovement = true;
            }
        }
    }

    //recebe do servidor um ack a dizer que ja disparou a sua ovelha
    private void OnReceiveAckShotMessage(NetworkMessage _message)
    {
        AckShotMessage _msg = _message.ReadMessage<AckShotMessage>();

        if (_msg.playerName == transform.name)//QUERO ACKs APENAS SOBRE OVELHAS QUE DISPAREI
        {
            numAcksShoot--;
            if (numAcksShoot == 0)
            {
                Debug.Log("(onreceive SHOT msg) Reactivanting Sending PlayerInfo");
                canSendNetworkMovement = true;
            }
        }
    }

    //recebe do servidor pedido de um jogador de se juntar a uma equipa
    private void OnReceiveJoinTeamMessage(NetworkMessage _message)
    {
        JoinTeamMessage _msg = _message.ReadMessage<JoinTeamMessage>();

        if(gameStateManagerObj != null)
            gameStateManagerObj.GetComponent<GameStateManager>().addToTeam(_msg.team, _msg.objectTransformName);
    }

    //recebe do servidor aviso que um jogador saiu do lobby
    private void OnReceiveLeaveLobbyMessage(NetworkMessage _message)
    {
        Debug.Log("Received Leave Lobby Message");

        LeaveLobbyMessage _msg = _message.ReadMessage<LeaveLobbyMessage>();

        if(_msg.objectTransformName != playerID)
            Manager.Instance.RemovePlayerFromConnectedPlayers(_msg.objectTransformName);
    }

    //recebe do servidor info dos outros players no lobby
    private void OnReceiveLobbyInfoMessage(NetworkMessage _message)
    {
        LobbyInfoMessage _msg = _message.ReadMessage<LobbyInfoMessage>();

        if(gameStateManagerObj != null)
            gameStateManagerObj.GetComponent<GameStateManager>().updateLobby(_msg);
    }

    //recebe do servidor info dos estado do jogo
    private void OnReceiveMatchInfoMessage(NetworkMessage _message)
    {
        MatchInfoMessage _msg = _message.ReadMessage<MatchInfoMessage>();

        if (gameStateManagerObj != null)
            gameStateManagerObj.GetComponent<GameStateManager>().updateMatch(_msg);
    }

    //recebe do servidor info que o jogo acabou e quem ganhou
    private void OnReceiveEndGameMessage(NetworkMessage _message)
    {
        //disable input

        EndGameMessage _msg = _message.ReadMessage<EndGameMessage>();

        bool foundSelf = false;

        if(!foundSelf && _msg.hasPlayer0 && _msg.player0transformName == playerID)
        {
            foundSelf = true;
            GameObject go = GameObject.Find("EndGameBannerEnabler");
            if (_msg.player0Won)
                go.GetComponent<EndGameBannerEnabler>().enableVictoryBanner();
            else
                go.GetComponent<EndGameBannerEnabler>().enableDefeatBanner();
        }
        else if (!foundSelf && _msg.hasPlayer1 && _msg.player1transformName == playerID)
        {
            foundSelf = true;
            GameObject go = GameObject.Find("EndGameBannerEnabler");
            if (_msg.player1Won)
                go.GetComponent<EndGameBannerEnabler>().enableVictoryBanner();
            else
                go.GetComponent<EndGameBannerEnabler>().enableDefeatBanner();
        }
        else if (!foundSelf && _msg.hasPlayer2 && _msg.player2transformName == playerID)
        {
            foundSelf = true;
            GameObject go = GameObject.Find("EndGameBannerEnabler");
            if (_msg.player2Won)
                go.GetComponent<EndGameBannerEnabler>().enableVictoryBanner();
            else
                go.GetComponent<EndGameBannerEnabler>().enableDefeatBanner();
        }
        else if (!foundSelf && _msg.hasPlayer3 && _msg.player3transformName == playerID)
        {
            foundSelf = true;
            GameObject go = GameObject.Find("EndGameBannerEnabler");
            if (_msg.player3Won)
                go.GetComponent<EndGameBannerEnabler>().enableVictoryBanner();
            else
                go.GetComponent<EndGameBannerEnabler>().enableDefeatBanner();
        }
        else if (!foundSelf && _msg.hasPlayer4 && _msg.player4transformName == playerID)
        {
            foundSelf = true;
            GameObject go = GameObject.Find("EndGameBannerEnabler");
            if (_msg.player4Won)
                go.GetComponent<EndGameBannerEnabler>().enableVictoryBanner();
            else
                go.GetComponent<EndGameBannerEnabler>().enableDefeatBanner();
        }
        else if (!foundSelf && _msg.hasPlayer5 && _msg.player5transformName == playerID)
        {
            foundSelf = true;
            GameObject go = GameObject.Find("EndGameBannerEnabler");
            if (_msg.player5Won)
                go.GetComponent<EndGameBannerEnabler>().enableVictoryBanner();
            else
                go.GetComponent<EndGameBannerEnabler>().enableDefeatBanner();
        }
    }

    //recebe do servidor movement dos outros players
    private void OnReceivePlayerInfoMessage(NetworkMessage _message)
    {
        PlayerInfoMessage _msg = _message.ReadMessage<PlayerInfoMessage>();

        //verifica se a mensagem NAO e sobre o proprio jogador
        if (_msg.playerName != transform.name)
        {
            //aceder ao player unit de quem enviou a mensagem e atualizar os valores desse jogador se este ja estiver instanciado
            if(Manager.Instance.ConnectedPlayers.ContainsKey(_msg.playerName))
                Manager.Instance.ConnectedPlayers[_msg.playerName].GetComponent<NetworkPlayer>().ProcessPlayerInfoMessage(_msg);
        }
    }

    //atualizar variaveis de lerping vindas de uma mensagem
    public void ProcessPlayerInfoMessage(PlayerInfoMessage _msg)
    {
        lastRealPosition = realPosition;
        lastRealRotation = realRotation;
        realPosition = _msg.playerPosition;
        realRotation = _msg.playerRotation;
        timeToLerp = _msg.time;

        if (realPosition != transform.GetChild(team - 1).transform.position)
        {
            isLerpingPosition = true;
        }

        if (realRotation.eulerAngles != transform.GetChild(team - 1).transform.rotation.eulerAngles)
        {
            isLerpingRotation = true;
        }

        //TO DO: SET THE playerAnimationState;

        updateSheepFromPlayerInfo(_msg);

        timeStartedLerping = Time.time;
    }

    public void updateSheepFromPlayerInfo(PlayerInfoMessage _msg)
    {
        GameObject[] sheepsToUpdate = new GameObject[_msg.numSheepCarried];
        for (int i = 0; i < _msg.numSheepCarried; i++)
        {
            sheepsToUpdate[i] = GameObject.Find(_msg.sheepNames[i]).gameObject;
            Debug.Log("SHEEPS TO UPDATE ROM PLAYER INFO:" + sheepsToUpdate[i].name);
        }

        for (int i = 0; i < _msg.numSheepCarried; i++)
        {
            GameObject sheep = sheepsToUpdate[i].gameObject;
            SheepMovement sheep_movement = sheep.GetComponent<SheepMovement>();
            Animator sheep_animator = sheep.GetComponentInChildren<Animator>();
            Rigidbody sheep_rb = sheep.GetComponent<Rigidbody>();

            //POSITION AND ROTATION
            sheep.transform.position = _msg.sheepPositions[i];
            sheep.transform.rotation = _msg.sheepRotations[i];
            sheep_movement.lastRealPosition = sheep_movement.realPosition;
            sheep_movement.lastRealRotation = sheep_movement.realRotation;
            sheep_movement.realPosition = sheep.transform.position;
            sheep_movement.realRotation = sheep.transform.rotation;

            //STATE
            Debug.Log("_msg.numSheepCarried: " + _msg.numSheepCarried);
            Debug.Log("_msg.sheepStates[" + i + "]: " + _msg.sheepStates[i] + " TEM DE SER 4");
            sheep_movement.setState(_msg.sheepStates[i]);

            //ANIMATION STATE
            IAnimState animState = obtainAnimState(_msg.sheepAnimationStates[i], ref sheep_animator);
            sheep_movement.SetAnimState(animState);

            //PICKED UP BY
            sheep_movement.setPickedUpBy(_msg.playerName);

            //GRAVITY
            sheep_rb.velocity = Vector3.zero;
            sheep_rb.angularVelocity = Vector3.zero;
            sheep_rb.useGravity = false;
            sheep_rb.isKinematic = true;
            sheep_movement.setGravity(false);

            //COLLISION BASES
            ignoreCollisionBases(sheep, true);
            sheep_movement.setIgnoreCollisionBases(true);

            //COLLISION PLAYERS
            ignoreCollisionPlayers(sheep, true);
            sheep_movement.setIgnoreCollisionPlayers(true);
        }
    }

    //recebe do servidor movement de todas as ovelhas que nao estao a ser controladas por ninguem
    private void OnReceiveSheepMovementMessage(NetworkMessage _message)
    {
        //server does not care about receiving sheep movement messages
        if (isServer)
        {
            Debug.Log("Servidor ia receber SheepMovementMessage mas retornou");
            return;
        }
            
        SheepMovementMessage _msg = _message.ReadMessage<SheepMovementMessage>();

        GameObject[] sheepsToUpdate = new GameObject[_msg.numAvailableSheep];
        for (int i = 0; i < sheepsToUpdate.Length; i++)
        {
            sheepsToUpdate[i] = GameObject.Find(_msg.sheepNames[i]).gameObject;
        }

        GameObject graphics = this.gameObject.transform.GetChild(team - 1).gameObject;
        for (int i = 0; i < sheepsToUpdate.Length; i++)
        {
            //prevent updating the values of a sheep that the player just picked up
            if (!graphics.GetComponent<PickupSheep>().getSheepStack().Contains(sheepsToUpdate[i]))
            {
                sheepsToUpdate[i].GetComponent<SheepMovement>().updateFromSheepMovement(_msg, i);
            }
        }
    }

    private void Update()
    {

        if(gameStateManagerObj == null)
            gameStateManagerObj = GameObject.FindGameObjectWithTag("GameStateManager");

        //verificar se ja e suposto enviar mensagem e se sim enviar
        if (isLocalPlayer && SceneManager.GetActiveScene().name == "CharacterThirdPerson")
        {
            if(sheepManager == null)
                sheepManager = GameObject.FindGameObjectWithTag("SheepManager");

            if (!canSendNetworkMovement)
            {
                canSendNetworkMovement = true;
                StartCoroutine(StartNetworkSendCooldown());
            }
        }
    }

    private IEnumerator StartNetworkSendCooldown()
    {
        timeBetweenMovementStart = Time.time;
        yield return new WaitForSeconds((1 / networkSendRate));
        SendPlayerInfoMessage();
    }

    private void SendPlayerInfoMessage()
    {
        timeBetweenMovementEnd = Time.time;

        GameObject graphics = this.gameObject.transform.GetChild(team - 1).gameObject;
        PickupSheep pickupsheep_script = graphics.GetComponent<PickupSheep>();
        Stack<GameObject> stack = pickupsheep_script.getSheepStack();
        m_animator = GetComponentInChildren<Animator>(); // TO DO: send player animation state. on ProcessPlayerInfoMessage, change animation if received state is different from previous

        PlayerInfoMessage _msg = new PlayerInfoMessage()
        {
            playerName = playerID,
            playerPosition = graphics.transform.position,
            playerRotation = graphics.transform.rotation,
            playerAnimationState = 1,//TO DO: CHANGE THIS. THIS IS A TEMPORARY VALUE
            numSheepCarried = stack.Count,
            sheepNames = getSheepNames(stack),
            sheepPositions = getSheepPositions(stack),
            sheepRotations = getSheepRotations(stack),
            sheepStates = getSheepStates(stack),
            sheepAnimationStates = getSheepAnimationStates(stack),
            time = (timeBetweenMovementEnd - timeBetweenMovementStart)
        };

        if (NetworkManager.singleton.client.isConnected)
            NetworkManager.singleton.client.Send(player_info_msg, _msg);

        canSendNetworkMovement = false;
    }

    private void FixedUpdate()
    {
        if (!isLocalPlayer)
        {
            NetworkLerp();
        }
    }

    //interpolaçao do movimento de players nao locais
    private void NetworkLerp()
    {
        if (isLerpingPosition)
        {
            float lerpPercentage = (Time.time - timeStartedLerping) / timeToLerp;

            this.transform.GetChild(team - 1).transform.position = Vector3.Lerp(lastRealPosition, realPosition, lerpPercentage);
        }

        if (isLerpingRotation)
        {
            float lerpPercentage = (Time.time - timeStartedLerping) / timeToLerp;

            this.transform.GetChild(team - 1).transform.rotation = Quaternion.Lerp(lastRealRotation, realRotation, lerpPercentage);
        }
    }

    public IAnimState obtainAnimState(int sheepAnimationState, ref Animator sheep_animator)//TO DO: CHECK IF ref SHOULD BE USED IN THE PARAMETER
    {
        switch (sheepAnimationState)
        {
            case (int)AnimStates.Shot:
                return new Shot(ref sheep_animator);
            case (int)AnimStates.Ball:
                return new Ball(ref sheep_animator);
            case (int)AnimStates.Walking:
                return new Walking(ref sheep_animator);
            case (int)AnimStates.Scared:
                return new Scared(ref sheep_animator);
            case (int)AnimStates.Iddle:
                return new Iddle(ref sheep_animator);
            default:
                Debug.Log("WRONG SHEEP ANIMATION STATE");
                return null;
        }
    }

    public string[] getSheepNames(Stack<GameObject> stack)
    {
        int num_sheep = stack.Count;
        string[] sheepNames = new string[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepNames[i] = stack.ToArray()[i].transform.name;
        }

        return sheepNames;
    }

    public Vector3[] getSheepPositions(Stack<GameObject> stack)
    {
        int num_sheep = stack.Count;
        Vector3[] sheepPositions = new Vector3[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepPositions[i] = stack.ToArray()[i].transform.position;
        }

        return sheepPositions;
    }

    public Quaternion[] getSheepRotations(Stack<GameObject> stack)
    {
        int num_sheep = stack.Count;
        Quaternion[] sheepRotations = new Quaternion[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepRotations[i] = stack.ToArray()[i].transform.rotation;
        }

        return sheepRotations;
    }

    public int[] getSheepStates(Stack<GameObject> stack)
    {
        int num_sheep = stack.Count;
        int[] sheepStates = new int[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepStates[i] = stack.ToArray()[i].GetComponent<SheepMovement>().getState();
            Debug.Log("sheepStates[i]: " + stack.ToArray()[i].GetComponent<SheepMovement>().getState() + "AQUI E 4 POHA");
        }

        return sheepStates;
    }

    public int[] getSheepAnimationStates(Stack<GameObject> stack)
    {
        int num_sheep = stack.Count;
        int[] sheepAnimationStates = new int[num_sheep];

        for (int i = 0; i < num_sheep; i++)
        {
            sheepAnimationStates[i] = stack.ToArray()[i].GetComponentInChildren<Animator>().GetInteger("Index");
        }

        return sheepAnimationStates;
    }

    public void setGraphicsActive(bool on)
    {
        if(on == false)
        {
            this.gameObject.transform.GetChild(0).gameObject.active = on;
            this.gameObject.transform.GetChild(1).gameObject.active = on;
            this.gameObject.transform.GetChild(2).gameObject.active = on;
            return;
        }


        GameObject go = this.gameObject.transform.GetChild(team - 1).gameObject;
        go.active = on;

        // Ignore collision between base and players
        GameObject[] baseWalls = GameObject.FindGameObjectsWithTag(Constants.BASE_WALL_TAG);
        Collider playerCollider = go.GetComponent<CapsuleCollider>();
        foreach (GameObject baseWall in baseWalls)
        {
            Collider c = GetComponentInChildren<Collider>();
            Physics.IgnoreCollision(baseWall.GetComponentInChildren<Collider>(), playerCollider);
        }


        //turn on gameSound
        GameObject gameSound = this.gameObject.transform.GetChild(4).gameObject;
        gameSound.active = on;

    }

    public void setTeam(int _team, int _teamIndex)
    {
        team = _team;
        teamIndex = _teamIndex;
        transform.GetComponentInChildren<FreeLookCam>().SetTarget(transform.GetChild(team - 1).transform);
        transform.GetComponentInChildren<FreeLookCam>().RealTarget = transform.GetChild(team - 1).transform;
        
        if(team == 1 && teamIndex == 0)
        {
            transform.GetChild(team - 1).transform.position = spawnPlayer0;
        }
        else if (team == 1 && teamIndex == 1)
        {
            transform.GetChild(team - 1).transform.position = spawnPlayer1;
        }
        else if (team == 2 && teamIndex == 0)
        {
            transform.GetChild(team - 1).transform.position = spawnPlayer2;
        }
        else if (team == 2 && teamIndex == 1)
        {
            transform.GetChild(team - 1).transform.position = spawnPlayer3;
        }
        else if (team == 3 && teamIndex == 0)
        {
            transform.GetChild(team - 1).transform.position = spawnPlayer4;
        }
        else if (team == 3 && teamIndex == 1)
        {
            transform.GetChild(team - 1).transform.position = spawnPlayer5;
        }

    }

    // ignore = true means: Avoid sheep from colliding with base walls
    // ignore = false means: Allow sheep to collide with base walls
    private void ignoreCollisionBases(GameObject sheep, bool ignore)
    {
        foreach (GameObject wall in GameObject.FindGameObjectsWithTag(Constants.BASE_WALL_TAG))
        {
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), wall.GetComponent<Collider>(), ignore);
        }
    }

    // ignore = true means: Avoid sheep from colliding with players
    // ignore = false means: Allow sheep to collide with players
    public void ignoreCollisionPlayers(GameObject sheep, bool ignore)
    {
        GameObject player;
        foreach (GameObject p in Manager.Instance.GetConnectedPlayers())
        {
            player = p.transform.GetChild(p.GetComponent<NetworkPlayer>().team - 1).gameObject;
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), player.GetComponent<Collider>(), ignore);
        }
    }
}
