﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXPlayer : MonoBehaviour
{

    GameObject gameStateManagerObj;

    [FMODUnity.EventRef]
    public string throwSheepAnnoucerEvent = "";
    [FMODUnity.EventRef]
    public string playerStunnedAnnoucerEvent = "";
    [FMODUnity.EventRef]
    public string pickUpSheepAnnouncerEvent = "";
    [FMODUnity.EventRef]
    public string pickUp3SheepAnnoucerEvent = "";
    [FMODUnity.EventRef]
    public string randomAnnoucerEvent = "";
    [FMODUnity.EventRef]
    public string winAnnoucerEvent = "";
    

    public float timer = 10.0f;

    public float randomTimer = 5.0f;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (gameStateManagerObj == null)
            gameStateManagerObj = GameObject.FindGameObjectWithTag("GameStateManager");


        if (timer > 0.0f)
        {
            timer -= Time.deltaTime;
            if (timer < 0.0f)
                timer = 0.0f;
        }

        randomTimer -= Time.deltaTime;
        if(randomTimer < 0)
        {
            randomTimer += 5.0f;
            if (Random.Range(0, 0.99f) < 0.2f && timer == 0.0f && gameStateManagerObj.GetComponent<GameStateManager>().time > 25)
            {
                FMODUnity.RuntimeManager.PlayOneShot(randomAnnoucerEvent);
                timer = 20.0f;
            }
        }

        
    }

    

    public void playThrowSheepAnnoucerEvent(Transform transform)
    {
        if (Random.Range(0, 0.99f) < 0.5f && timer == 0.0f && gameStateManagerObj.GetComponent<GameStateManager>().time > 25)
        {
            FMODUnity.RuntimeManager.PlayOneShot(throwSheepAnnoucerEvent, transform.position);
            timer = 10.0f;
        }
    }

    public void playPlayerStunnedAnnoucerEvent(Transform transform)
    {
        if (Random.Range(0, 0.99f) < 0.5f && timer == 0.0f && gameStateManagerObj.GetComponent<GameStateManager>().time > 25)
        {
            FMODUnity.RuntimeManager.PlayOneShot(playerStunnedAnnoucerEvent, transform.position);
            timer = 10.0f;
        }
    }

    public void playPickUpSheepAnnouncerEvent(Transform transform)
    {
        if (Random.Range(0, 0.99f) < 0.3f && timer == 0.0f && gameStateManagerObj.GetComponent<GameStateManager>().time > 25)
        {
            FMODUnity.RuntimeManager.PlayOneShot(pickUpSheepAnnouncerEvent, transform.position);
            timer = 10.0f;
        }   
    }

    public void playPickUp3SheepAnnoucerEvent(Transform transform)
    {
        if (Random.Range(0, 0.99f) < 0.8f && timer == 0.0f && gameStateManagerObj.GetComponent<GameStateManager>().time > 25)
        {
            FMODUnity.RuntimeManager.PlayOneShot(pickUp3SheepAnnoucerEvent, transform.position);
            timer = 10.0f;
        }              
    }

    public void playWinAnnoucerEvent()
    {
        FMODUnity.RuntimeManager.PlayOneShot(winAnnoucerEvent);
        timer = 20.0f;
    }
}
