﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;
using SheepAnimationState;
using PlayerManager;

public class SheepMovement : NetworkMessageHandler
{
    private Rigidbody m_Rigidbody;
    private Vector3 targetDestination;
    private Quaternion targetRotation;
    private int state = (int)State.Available;
    private string pickedUpBy = "";
    public int justShot = 0; //ONLY ON CLIENT TO PREVENT SELF STUN
    private IAnimState animState;
    private Animator m_animator;
    // private TerrainData m_arena;
    private GameObject[] baseWalls;
    private bool usingGravity = true;
    private bool ignoreCollisionBases = false;
    private bool ignoreCollisionPlayers = true;

    private const float ROTATION_SPEED = 500f;
    private const float MOVING_SPEED = 5f;
    private const float SCARED_ROTATION_SPEED = 1000f;
    private const float SCARED_MOVING_SPEED = 2f;
    private const float SCARE_DISTANCE = 4f;
    private const float sheepFeetFromFloor = 2;

    [Header("Sheep Properties")]
    public string sheepID;

    [Header("Lerping Properties")]
    public bool isLerpingPosition;
    public bool isLerpingRotation;
    public Vector3 realPosition;
    public Quaternion realRotation;
    public Vector3 lastRealPosition;
    public Quaternion lastRealRotation;
    public float timeStartedLerping;
    public float timeToLerp;

    [FMODUnity.EventRef]
    public string sheepScaredEvent = "";

    public float soundTimer = 0.0f;

    // Start is called before the first frame update
    void Start()
    {
        sheepID = "sheep" + GetComponent<NetworkIdentity>().netId.ToString();
        transform.name = sheepID;

        m_Rigidbody = GetComponent<Rigidbody>();
        m_animator = GetComponentInChildren<Animator>();
        animState = new Iddle(ref m_animator);
        // m_arena = GameObject.FindGameObjectWithTag("Arena").GetComponent<Terrain>().terrainData;
        baseWalls = GameObject.FindGameObjectsWithTag(Constants.BASE_WALL_TAG);
    }

    // Update is called once per frame
    void FixedUpdate()
    {

        if (soundTimer > 0.0f)
        {
            soundTimer -= Time.fixedDeltaTime;
            if (soundTimer < 0.0f)
                soundTimer = 0.0f;
        }

        if (state == (int)State.Scared && soundTimer == 0.0f)
        {
            FMODUnity.RuntimeManager.PlayOneShot(sheepScaredEvent, transform.position);
            soundTimer = 6.0f;
        }
        if(justShot >= 1)
            justShot--;
        Debug.Log("justShot = " + justShot);
        if (!isServer)
        {
            NetworkLerp();
            return;
        }
        /*else if (state == (int)State.Unavailable){ //servidor so faz lerp se pickedup sheeps (unica situacao em que cliente tem autoridade sobre elas)
            NetworkLerp();
            return;
        }*/

        if (state == (int)State.Unavailable)
        {
            Debug.Log("estado Unavailable: " + name);
            return;
        }

        GameObject closestPlayer = getClosestPlayer();
        if(closestPlayer != null)
        {
            if(canBeScared())
            {
                var opposite_direction = transform.position - closestPlayer.transform.position;
                scare(opposite_direction);
            }
        }
        else if(state == (int)State.Scared)
        {
            unscare();
        }

        switch (state)
        {
            case (int)State.Rotating:
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, ROTATION_SPEED * Time.fixedDeltaTime);
                float angle = Quaternion.Angle(transform.rotation, targetRotation);
                if (angle == 0)
                    state = (int)State.Moving;

                break;

            case (int)State.Moving:

                transform.position = Vector3.MoveTowards(transform.position, targetDestination, MOVING_SPEED * Time.fixedDeltaTime);
                if (Vector3.Distance(transform.position, targetDestination) < 0.01f)
                    state = (int)State.Waiting;

                break;

            case (int)State.Scared:
                transform.rotation = Quaternion.RotateTowards(transform.rotation, targetRotation, SCARED_ROTATION_SPEED * Time.fixedDeltaTime);
                transform.position = Vector3.MoveTowards(transform.position, targetDestination, SCARED_MOVING_SPEED * Time.fixedDeltaTime);
                
                break;
        }

        animState = animState.next(state);
    }

    float getHeightOfTerrainAt()
    {
        return Terrain.activeTerrain.SampleHeight(transform.position);
    }

    public GameObject getClosestPlayer()
    {
        GameObject closestPlayer = null;
        float minDistance = -1f;
        float distance;
        GameObject player;

        foreach (GameObject p in Manager.Instance.GetConnectedPlayers())
        {
            player = p.transform.GetChild(p.GetComponent<NetworkPlayer>().team - 1).gameObject; // Gets the correct Graphics according to the player's team
            distance = Vector3.Distance(player.transform.position, transform.position);
    
            if (distance <= SCARE_DISTANCE)
            {
                if(distance <= minDistance || minDistance == -1f)
                {
                    minDistance = distance;
                    closestPlayer = player;
                }
            }
        }

        return closestPlayer;
    }

    public bool canBeScared()
    {
        return state != (int)State.Unavailable && state != (int)State.Flying;
    }
    
    public IEnumerator move()
    {
        float x_displace = Random.Range(-10.0f, 10.0f);
        float z_displace = Random.Range(-10.0f, 10.0f);
        Vector3 displacement = new Vector3(x_displace, 0f, z_displace);

        targetDestination = transform.position + displacement;
        targetRotation = Quaternion.LookRotation(transform.position - targetDestination);

        state = (int)State.Rotating;
        yield return new WaitUntil(() => state == (int)State.Moving); //wait until rotation finishes

        state = (int)State.Moving;
        yield return new WaitUntil(() => state == (int)State.Waiting); //wait until movement finishes

        float stop_time = Random.Range(4f, 6f);
        yield return new WaitForSeconds(stop_time); // Number of seconds this sheep will wait after it completed its movement, before being able to move again
        if (state == (int)State.Waiting) // the state might be Unavaible if it was picked up while moving
            state = (int)State.Available;
    }

    public void scare(Vector3 direction)//TO DO: test + change to continuous movement
    {
        float magnitude = Mathf.Sqrt((direction.x * direction.x) + (direction.z * direction.z));

        float x_normalized = direction.x / magnitude;
        float z_normalized = direction.z / magnitude;

        float x_intensity = 2f;
        float z_intensity = 2f;

        Vector3 displacement = new Vector3(x_intensity * x_normalized, 0.0f, z_intensity * z_normalized);

        // immediatly after dropping sheep, for example, the x and z values of displacement could be NaN due to the sheep being directly above the player
        if(float.IsNaN(displacement.x) || float.IsNaN(displacement.z))
        {
            displacement = new Vector3(0.0f, 0.0f, 0.0f);
        }

        targetDestination = transform.position + displacement;
        targetRotation = Quaternion.LookRotation(transform.position - targetDestination);

        state = (int)State.Scared;
    }

    public void unscare()
    {
        state = (int)State.Available;
    }

    public void updateFromSheepMovement(SheepMovementMessage _msg, int sheepIndex)
    {
        //LERP POSITION AND ROTATION
        lastRealPosition = realPosition;
        lastRealRotation = realRotation;
        realPosition = _msg.sheepPositions[sheepIndex];
        realRotation = _msg.sheepRotations[sheepIndex];
        timeToLerp = _msg.time;

        if (realPosition != transform.position)
        {
            isLerpingPosition = true;
        }

        if (realRotation.eulerAngles != transform.rotation.eulerAngles)
        {
            isLerpingRotation = true;
        }

        timeStartedLerping = Time.time;

        //STATE
        setState(_msg.sheepStates[sheepIndex]);

        //ANIMATION STATE
        int previous_anim_index = m_animator.GetInteger("Index");
        int anim_index = _msg.sheepAnimationStates[sheepIndex];
        if (previous_anim_index != anim_index)
        {
            m_animator.SetInteger("Index", anim_index);
            m_animator.SetTrigger("Next");
        }

        //PICKED UP BY
        setPickedUpBy("");

        //GRAVITY
        m_Rigidbody.useGravity = true;
        m_Rigidbody.isKinematic = false;
        setGravity(true);

        //COLLISION BASES
        ignoreCollisionWithBases(transform.gameObject, true);
        setIgnoreCollisionBases(true);

        //COLLISION PLAYERS
        ignoreCollisionWithPlayers(transform.gameObject, _msg.sheepCollisionPlayers[sheepIndex]);
        setIgnoreCollisionPlayers(_msg.sheepCollisionPlayers[sheepIndex]);
    }

    private void NetworkLerp()
    {
        if (isLerpingPosition)
        {
            float lerpPercentage = (Time.time - timeStartedLerping) / timeToLerp;
            transform.position = Vector3.Lerp(lastRealPosition, realPosition, lerpPercentage);
        }

        if (isLerpingRotation)
        {
            float lerpPercentage = (Time.time - timeStartedLerping) / timeToLerp;

            transform.rotation = Quaternion.Lerp(lastRealRotation, realRotation, lerpPercentage);
        }
    }

    public bool getGravity()
    {
        return usingGravity;
    }

    public void setGravity(bool gravity)
    {
        usingGravity = gravity;
    }

    public bool getIgnoreCollisionBases()
    {
        return ignoreCollisionBases;
    }

    public void setIgnoreCollisionBases(bool ignore)
    {
        ignoreCollisionBases = ignore;
    }

    public bool getIgnoreCollisionPlayers()
    {
        return ignoreCollisionPlayers;
    }
    public void setIgnoreCollisionPlayers(bool ignore)
    {
        ignoreCollisionPlayers = ignore;
    }

    public int getState()
    {
        return state;
    }

    public void setState(int state)
    {
        this.state = state;
    }

    public string getPickedUpBy()
    {
        return pickedUpBy;
    }

    public void setPickedUpBy(string pickedUpBy)
    {
        this.pickedUpBy = pickedUpBy;
    }

    public void setAvailable()
    {
        this.state = (int)State.Available;
    }

    public void setFlying()
    {
        this.state = (int)State.Flying;
    }

    public void setUnavailable()
    {
        this.state = (int)State.Unavailable;
    }

    public void SetAnimState(IAnimState animStateArg)
    {
        animState = animStateArg;
    }

    public void ignoreCollisionWithBases(GameObject sheep, bool ignore)
    {
        foreach (GameObject wall in baseWalls)
        {
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), wall.GetComponent<Collider>(), ignore);
        }
    }

    public void ignoreCollisionWithPlayers(GameObject sheep, bool ignore)
    {
        GameObject player;
        foreach (GameObject p in Manager.Instance.GetConnectedPlayers())
        {
            player = p.transform.GetChild(p.GetComponent<NetworkPlayer>().team - 1).gameObject;
            Physics.IgnoreCollision(sheep.GetComponent<Collider>(), player.GetComponent<Collider>(), ignore);
        }
    }
}